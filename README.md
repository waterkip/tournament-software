# DESCRIPTION

This module creates a tournament bracket for single elimination tournaments. It
creates the first set of matches that are played.

# SYNOPSIS

    my $tournament = Games::Tournament::Format::Elimination::Single->new(
      draw => 8, # mandatory, min of 8

      seeded => \@list_of_seeded_players,
      unseeded => \@list_of_unseeded_players,

      wildcards => \@list_of_wildcards,
      qualifiers => \@list_of_qualifiers,
    );

    $tournament->seed;

    my @left_side  = $tournament->matches('left');
    my @right_side = $tournament->matches('right');
    my @both       = $tournament->matches();

# METHODS

## new

    my $tournament = Games::Tournament::Format::Elimination::Single->new(
      draw => 8, # mandatory, min of 8

      seeded => \@list_of_seeded_players,
      unseeded => \@list_of_unseeded_players,

      wildcards => \@list_of_wildcards,
      qualifiers => \@list_of_qualifiers,
    );

The following arguments can be fed to new

- draw

    The draw size, minimum of 8, must be a power of 2

- seeded

    An array ref of seeded players. Order determines the seeding order.

- unseeded

    An array ref of unseeded players. If you want randomisation, you should
    randomize it prior to feeding it to `new`.

- wildcards

    An array ref of wildcard players. If you want randomisation, you should
    randomize it prior to feeding it to `new`.

- qualifiers

    An array ref of qualifier players. If you want randomisation, you should
    randomize it prior to feeding it to `new`.

These arguments can tweak the seeding process

- weakest-seed

    The highest seeded players will always play the lowest seeded players when
    all-seeded tournaments are played. For example, when a 8 draw tournament is
    seeded with 8 seeded players, seed 1 will play seed 8, seed 2 will play 7, etc.
    When set to **false** the seeding will be done more equal, where seed 1 will
    play seed 5, seed 2 will play 6, etc. Defaults to **true**.

- weakest-opp

    The highest seeded players will always face a much lower seeded opponent in
    case they progress. When set to **false** the opponent will still be a lower
    seed, but the difference is smaller. This mostly has effect in the second or
    third round of tournaments. The impact is the greatest when **weakest-seed** is
    also set to false. Defaults to **true**.

- qualifier-seed

    This will treat qualifiers as seeded when set to **true**. Defaults to **false**.

- wildcard-seed

    This will treat wildcards as seeded when set to **true**. Defaults to **false**.

- order

    This is an array ref which changes the order of how the seeding is done. The
    default order is to first add the unseeded players to the matches against the
    seeded players, than the qualifiers and the wildcards at the latest moment.

    Changing the order will affect things. It currently also changes which groups
    gets "Byes" in case there aren't enought seeded players for "Byes".

## matches

    $self->matches('left');  # similar to 'bottom'
    $self->matches('right'); # similar to 'top'
    $self->matches();        # all matches, both sides

    Return a list of matches

## player\_count

Return the amount of players

## number\_of\_rounds

Returns the number of rounds

## byes

Returns amount of byes

## rounds

Returns the rounds in a somewhat human readable format

## round

Returns the round based on the ID, either the numeric ID or the ID which you
get from `rounds`.

## games

Returns the amount of games
