package Games::Tournament::Format::Elimination::Single;
use v5.26;
use Object::Pad;

# ABSTRACT: Automatic tournament seeding software

=head1 DESCRIPTION

This module creates a tournament bracket for single elimination tournaments. It
creates the first set of matches that are played.

=head1 SYNOPSIS

  my $tournament = Games::Tournament::Format::Elimination::Single->new(
    draw => 8, # mandatory, min of 8

    seeded => \@list_of_seeded_players,
    unseeded => \@list_of_unseeded_players,

    wildcards => \@list_of_wildcards,
    qualifiers => \@list_of_qualifiers,
  );

  $tournament->seed;

  my @left_side  = $tournament->matches('left');
  my @right_side = $tournament->matches('right');
  my @both       = $tournament->matches();

=cut

class Games::Tournament::Format::Elimination::Single;
use Carp qw(croak);
use List::Util qw(first any none);

field $draw :param;
field $weakest_seed :param(weakest-seed) //= 1;
field $weakest_opponent :param(weakest-opp) //= 1;
field $wildcard_as_seed :param(wildcard-seed) //= 0;
field $qualifier_as_seed :param(qualifier-seed) //= 0;

field @seeded;
field @unseeded;
field @wildcards;
field @qualifiers;
field @byes;
field @players;

field $byes = 0;
field $player_count = 0;

field @rounds;
field $rounds = 0;
field @matches;
field @left;
field @right;

field @seeded_matches;
field @unseeded_matches;

field @order;

field $unseeded_idx = 0;
field $qualifier_idx = 0;
field $wildcard_idx = 0;

field $max_matches;
field $max_bracket_matches;

=head1 METHODS

=head2 new

  my $tournament = Games::Tournament::Format::Elimination::Single->new(
    draw => 8, # mandatory, min of 8

    seeded => \@list_of_seeded_players,
    unseeded => \@list_of_unseeded_players,

    wildcards => \@list_of_wildcards,
    qualifiers => \@list_of_qualifiers,
  );

The following arguments can be fed to new

=over

=item draw

The draw size, minimum of 8, must be a power of 2

=item seeded

An array ref of seeded players. Order determines the seeding order.

=item unseeded

An array ref of unseeded players. If you want randomisation, you should
randomize it prior to feeding it to C<new>.

=item wildcards

An array ref of wildcard players. If you want randomisation, you should
randomize it prior to feeding it to C<new>.

=item qualifiers

An array ref of qualifier players. If you want randomisation, you should
randomize it prior to feeding it to C<new>.

=back

These arguments can tweak the seeding process

=over

=item weakest-seed

The highest seeded players will always play the lowest seeded players when
all-seeded tournaments are played. For example, when a 8 draw tournament is
seeded with 8 seeded players, seed 1 will play seed 8, seed 2 will play 7, etc.
When set to B<false> the seeding will be done more equal, where seed 1 will
play seed 5, seed 2 will play 6, etc. Defaults to B<true>.

=item weakest-opp

The highest seeded players will always face a much lower seeded opponent in
case they progress. When set to B<false> the opponent will still be a lower
seed, but the difference is smaller. This mostly has effect in the second or
third round of tournaments. The impact is the greatest when B<weakest-seed> is
also set to false. Defaults to B<true>.

=item qualifier-seed

This will treat qualifiers as seeded when set to B<true>. Defaults to B<false>.

=item wildcard-seed

This will treat wildcards as seeded when set to B<true>. Defaults to B<false>.

=item order

This is an array ref which changes the order of how the seeding is done. The
default order is to first add the unseeded players to the matches against the
seeded players, than the qualifiers and the wildcards at the latest moment.

Changing the order will affect things. It currently also changes which groups
gets "Byes" in case there aren't enought seeded players for "Byes".

=back

=cut

sub BUILDARGS {
  my $self = shift;

  my %args = @_;

  # uncoverable branch false
  if (my $draw = $args{draw}) {
    croak "Unable to set draw of $draw, not a power of 2" unless is_pow_of_two($draw);
    croak "Unable to set draw with a size less than 8" if $draw < 8;
  }

  if (!exists $args{seeded} && !exists $args{unseeded}) {
    croak "No seeded or unseeded participants";
  }

  foreach (qw(seeded unseeded qualifiers wildcards)) {
    $args{$_} //= [];
  }

  my @default = qw(unseeded qualifier wildcard);

  # TODO: enum for order
  $args{order} //= \@default;
  my @order;
  foreach my $key (@{$args{order}}) {
    push(@order, $key) if any { $_ eq $key } @default;
  }
  $args{order} = \@order;

  return (%args);
}

ADJUSTPARAMS {
  my $args = shift;

  $max_matches = $draw / 2;
  $max_bracket_matches = $max_matches / 2;

  push(@order, $_) foreach (@{$args->{order}});

  $self->_add_seeded(@{ $args->{seeded} });
  $self->_add_unseeded(@{ $args->{unseeded} });
  $self->_add_qualifiers(@{ $args->{qualifiers} });
  $self->_add_wildcards(@{ $args->{wildcards} });

  croak("Cannot have a tourney without participants") unless $player_count;
  croak("Cannot have a tourney with more players than the draw size!")
    if $player_count > $draw;

  $self->_add_bye_players;

  $rounds = pow_of_two($draw);

  @rounds = qw(F SF QF);

  if ($rounds > 3) {
    my $p = 16;
    foreach (4..$rounds) {
      push(@rounds, "R$p");
      $p *= 2;
    }
  }

  @rounds = reverse(@rounds);

  $self->_init_seeded_matches;
  $self->_bracket_matches;
  $self->_add_matches;
}

method _init_seeded_matches {

  $self->_add_bye_matches;

  foreach (@order) {
    $self->_add_seeded_matches($_, @unseeded)   if $_ eq 'unseeded';
    $self->_add_seeded_matches($_, @qualifiers) if $_ eq 'qualifier';
    $self->_add_seeded_matches($_, @wildcards)  if $_ eq 'wildcard';
  }

  my @l     = @seeded;
  my @right = splice(@l, $max_matches);
  @right = reverse(@right) if $weakest_seed;
  $self->_add_seeded_matches('seeded', @right);

  @matches = @seeded_matches;
  return;
}

method _add_bye_matches {

  return unless $byes;
  my $max = @seeded - $byes;

  if ($max < 0) {
    # Move others into the seeded area for bye handling
    $max *= -1;
    foreach (@order) {
      my $source = \@unseeded   if $_ eq 'unseeded';
      $source = \@wildcards  if $_ eq 'wildcard';
      $source = \@qualifiers if $_ eq 'qualifier';
      if (@$source >= $max) {
        my @grab = splice(@$source, 0, $max);
        push(@seeded, @grab);
        $max = 0;
      }
      elsif (@$source < $max) {
        push(@seeded, @$source);
        $max -= @$source;
        @$source = ();
      }
    }
  }

  for my $i (1..$byes) {
    last unless defined $seeded[$i-1];
    push(@seeded_matches, "$seeded[$i-1] vs Bye");
    pop @byes;
  }

}

method _add_seeded_matches($src, @source) {

  return if !@seeded;
  return if @seeded_matches == @seeded;

  my $idx = @seeded_matches;
  my $max = @seeded - @seeded_matches + $idx;

  $max = @source if @source < $max;

  my $i;
  for ($i = 0; $i < $max; $i++) {
    my $s = shift @source;
    push(@seeded_matches, "$seeded[$i + $idx] vs $s");
    last if @seeded_matches == @seeded;
  }

  $unseeded_idx  = $i if $src eq 'unseeded';
  $wildcard_idx  = $i if $src eq 'wildcard';
  $qualifier_idx = $i if $src eq 'qualifier';
}


method _add_unseeded_matches($src, $max, @source) {

  return unless $max;
  return unless @source;

  my $idx = $unseeded_idx if $src eq 'unseeded';
  $idx = $wildcard_idx if $src eq 'wildcard';
  $idx = $qualifier_idx if $src eq 'qualifier';

  splice(@source, 0, $idx);

  if (@source > $max) {
    splice(@source, $max);
  }
  push(@unseeded_matches, @source);

  $unseeded_idx  += @source if $src eq 'unseeded';
  $wildcard_idx  += @source if $src eq 'wildcard';
  $qualifier_idx += @source if $src eq 'qualifier';

  return $max - @source;

}

method _add_unseeded_matches_opp($src, $idx, @source) {
  return $idx unless @source;
  my $max = @unseeded_matches - $idx;
  return $idx unless $max;

  my $s = $unseeded_idx if $src eq 'unseeded';
  $s = $wildcard_idx if $src eq 'wildcard';
  $s = $qualifier_idx if $src eq 'qualifier';


  splice(@source, 0, $s);
  if (@source > $max) {
    splice(@source, $max);
  }

  return $idx unless @source;

  foreach (@source) {
    $unseeded_matches[$idx] .= ' vs ' . $_;
    $idx++
  }

  $unseeded_idx  += @source if $src eq 'unseeded';
  $wildcard_idx  += @source if $src eq 'wildcard';
  $qualifier_idx += @source if $src eq 'qualifier';
  return $idx;
}

method _add_matches(@source) {

  return if @matches == $max_matches;

  my $m_left  = $max_bracket_matches - @left;
  my $m_right = $max_bracket_matches - @right;
  my $matches_to_add = $m_left + $m_right;


  for (@order) {
    $matches_to_add = $self->_add_unseeded_matches($_, $matches_to_add, @unseeded) if $_ eq 'unseeded';
    $matches_to_add = $self->_add_unseeded_matches($_, $matches_to_add, @wildcards) if $_ eq 'wildcard';
    $matches_to_add = $self->_add_unseeded_matches($_, $matches_to_add, @qualifiers) if $_ eq 'qualifier';
  }

  my $idx = 0;
  for (@order) {
    $idx = $self->_add_unseeded_matches_opp($_, $idx, @unseeded) if $_ eq 'unseeded';
    $idx = $self->_add_unseeded_matches_opp($_, $idx, @wildcards) if $_ eq 'wildcard';
    $idx = $self->_add_unseeded_matches_opp($_, $idx, @qualifiers) if $_ eq 'qualifier';
  }

  while (@byes) {
    splice(@byes, -2);
    push(@unseeded_matches, "Bye vs Bye");
  }

  $idx = 1;
  my $s = @left > @right ? -1 : 1;
  my $c = 0;

  while (@unseeded_matches) {
      my $match = shift @unseeded_matches;

      my $source = $s > 0 ? \@left : \@right;
      @$source ? splice(@$source, $idx, 0, $match) : push(@$source, $match);

      $s *= -1;
      # Flip where we insert next item
      if ($c % 2) {
        $idx *= -1;
        $idx += 2 if $idx > 0;
        $idx = 1 if $idx > @left / 2;
      }
      $c++;
  }

  @matches = ();
  for (my $i = 0; $i < @left; $i++) {
    push(@matches, $left[$i], $right[$i]);
  }

}

method _bracket_matches {

  while (@matches) {
    push(@left, shift(@matches));
    push(@right, shift(@matches)) if @matches;
  }

  # The right side (which has the no 2 seed ALWAYS has the extra seeding so
  # seed 1 encounters less seeded players. Unless there is one seed (edge case)
  # which stays on the left and we use the right side for all the unseeded
  # players
  if (@left > 1 && @left > @right) {
    push(@right, pop @left);
  }

  @left  = $self->_shuffle_bracket(@left);
  @right = $self->_shuffle_bracket(@right);

  for (my $i = 0; $i < @left; $i+=2) {
     push(@matches, $left[$i]) if $left[$i];
     push(@matches, $right[$i]) if $right[$i];

     push(@matches, $left[$i+1]) if $left[$i+1];
     push(@matches, $right[$i+1]) if $right[$i+1];
  }

}

method _shuffle_bracket(@bracket) {

  my @l;
  my @r;
  while (@bracket) {
    push(@l, shift(@bracket), pop(@bracket));
    push(@r, shift(@bracket), pop(@bracket));

    # Decide to flip/flop each match order to get a correct bracket order for
    # when we prioritize seeded players to reach the finals. If there are no
    # matches left we skip the whole lot too.
    #
    # TODO: Look more into this logic
    # This only has effect when a big enough player size is seeded. Otherwise
    # it has very little effect. For example, a 128 draw with 20 seeds is
    # hardly influenced by this logic. I think the tipping point is > 25% seeds
    # In the bigger draws (64 and up) it seems to primarly affect the seeds
    # around 16-24
    #
    # It is a tweak to alter the seeding, but perhaps we should make the next
    # default
    next if !$weakest_opponent || !@bracket;
    my @b = splice(@bracket, int(@bracket / 2));
    @bracket = (reverse(@bracket), reverse(@b));
  }

  # TODO: Look more into this logic
  # Reversing or not reversing here also has little effect on the actual draw
  return (grep { defined } @l, reverse(@r));
}

=head2 matches

  $self->matches('left');  # similar to 'bottom'
  $self->matches('right'); # similar to 'top'
  $self->matches();        # all matches, both sides

  Return a list of matches

=cut

method matches($side = undef) {

  return @matches unless $side;
  return @right if any { $side eq $_ } qw(bottom right);
  return @left if any { $side eq $_ } qw(top left);
  return @matches;
}


method _add_unseeded {
  push(@unseeded, @_);
  push(@order, 'unseeded') if none { $_ eq 'unseeded' } @order;
  $player_count++ for @_;
}

method _add_seeded {
  push(@seeded, @_);
  $player_count++ for @_;
}

method _add_wildcards {
  if ($wildcard_as_seed) {
    $self->_add_seeded(@_);
    return;
  }

  push(@wildcards, @_);
  push(@order, 'wildcard') if none { $_ eq 'wildcard' } @order;
  $player_count++ for @_;
}

method _add_qualifiers {
  if ($qualifier_as_seed) {
    $self->_add_seeded(@_);
    return;
  }
  push(@qualifiers, @_);
  push(@order, 'qualifier') if none { $_ eq 'qualifier' } @order;
  $player_count++ for @_;
}

=head2 player_count

Return the amount of players

=cut

method player_count() {
  return $player_count;
}

=head2 number_of_rounds

Returns the number of rounds

=cut

method number_of_rounds() {
  return $rounds;
}

=head2 byes

Returns amount of byes

=cut

method byes() {
  return $byes;
}

=head2 rounds

Returns the rounds in a somewhat human readable format

=cut

method rounds() {
  return @rounds;
}

=head2 round

Returns the round based on the ID, either the numeric ID or the ID which you
get from C<rounds>.

=cut

method round($ask) {

  if ($ask =~ /^[0-9]$/) {
    croak "Unable to ask for such a round" if $ask > $rounds;
    return $rounds[$ask -1];
  }

  my $idx = first { $_ eq uc($ask) } @rounds;
  return $idx if $idx;

  croak "Unable to determine what you want";


}

=head2 games

Returns the amount of games

=cut


method games {
  my $count = $self->player_count -1;
  my @array = (1..$count);
  return @array;
}

method _add_bye_players {
  $byes = $draw - $player_count;
  @byes = (1..$byes);
}

sub is_pow_of_two {
  my $val = shift;
  return (!($val & ($val - 1)) && $val) ? 1 : 0;
}

sub pow_of_two {
  my $val = shift;
  my $count = 0;
  while ($val != 1) {
    $val = $val >> 1;
    $count++;
  }
  return $count;
}

1;

__END__
