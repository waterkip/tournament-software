use strict;
use warnings;
use Test::More 0.96;
use Test::Exception;
use Test::Deep;

use Games::Tournament::Format::Elimination::Single;

my $DEBUG = 0;

sub _get_seeder {
  my %opts = @_;

  my $draw   = delete $opts{draw}   // 16;
  my $seeded = delete $opts{seeded} // $draw / 2;

  my @seeded = map { "Seed $_" } (1 .. $seeded);

  my @qualifiers;
  if (my $q = delete $opts{qualifiers}) {
    @qualifiers = map { "Qualifier $_" } (1 .. $q);
  }
  my @wildcards;
  if (my $q = delete $opts{wildcards}) {
    @wildcards = map { "Wild card $_" } (1 .. $q);
  }

  my $byes = delete $opts{byes} // 0;

  my @unseeded;
  if (defined $opts{unseeded}) {
    @unseeded = map { "Unseeded $_" } (1 .. $opts{unseeded});
  }
  elsif (!exists $opts{unseeded}) {
    my $diff = $draw - $seeded - @wildcards - @qualifiers - $byes;
    @unseeded = map { "Unseeded $_" } (1 .. $diff) if $diff > 0;
  }
  delete $opts{unseeded};


  return Games::Tournament::Format::Elimination::Single->new(
    draw       => $draw,
    seeded     => \@seeded,
    unseeded   => \@unseeded,
    qualifiers => \@qualifiers,
    wildcards  => \@wildcards,
    %opts,
  );

}

sub test_matches {
  my $t      = shift;
  my $opts   = shift if @_ == 3;
  my $expect = shift;
  my $msg    = shift;

  my @matches = $t->matches($opts);
  my $ok      = cmp_deeply(\@matches, $expect, $msg);

  return $ok if $ok;

  diag explain \@matches if !$ok;
  diag explain $expect;

  return $ok;
}

{
  my $t = _get_seeder(
    draw   => 8,
    seeded => 8,
  );

  test_matches(
    $t,
    [
      'Seed 1 vs Seed 8',
      'Seed 2 vs Seed 7',
      'Seed 3 vs Seed 6',
      'Seed 4 vs Seed 5'
    ],
    "Smallest draw possible"
  );
}

{
  my $t = _get_seeder(
    draw     => 8,
    seeded   => 7,
    byes     => 1
  );

  test_matches(
    $t,
    [
      'Seed 1 vs Bye',
      'Seed 2 vs Seed 7',
      'Seed 3 vs Seed 6',
      'Seed 4 vs Seed 5'
    ],
    "... and with bye"
  );

}


{
  my $t = _get_seeder(seeded => 8);

  test_matches(
    $t,
    [
      'Seed 1 vs Unseeded 1',
      'Seed 2 vs Unseeded 2',
      'Seed 7 vs Unseeded 7',
      'Seed 8 vs Unseeded 8',
      'Seed 5 vs Unseeded 5',
      'Seed 6 vs Unseeded 6',
      'Seed 3 vs Unseeded 3',
      'Seed 4 vs Unseeded 4',
    ],
    "16 draw with 8 seeded and 8 unseeded"
  );

}

{
  my $t = _get_seeder(
    seeded   => 8,
    unseeded => 0
  );

  test_matches(
    $t,
    [
      'Seed 1 vs Bye',
      'Seed 2 vs Bye',
      'Seed 7 vs Bye',
      'Seed 8 vs Bye',
      'Seed 5 vs Bye',
      'Seed 6 vs Bye',
      'Seed 3 vs Bye',
      'Seed 4 vs Bye',
    ],
    "16 draw with 8 seeded and 8 byes"
  );

  test_matches(
    $t, 'left',
    ['Seed 1 vs Bye', 'Seed 7 vs Bye', 'Seed 5 vs Bye', 'Seed 3 vs Bye',],
    "... and correct left hand side"
  );
  test_matches(
    $t, 'top',
    ['Seed 1 vs Bye', 'Seed 7 vs Bye', 'Seed 5 vs Bye', 'Seed 3 vs Bye',],
    "... and is the same as bottom"
  );

  test_matches(
    $t, 'right',
    ['Seed 2 vs Bye', 'Seed 8 vs Bye', 'Seed 6 vs Bye', 'Seed 4 vs Bye',],
    "... and correct right hand side"
  );

  test_matches(
    $t, 'bottom',
    ['Seed 2 vs Bye', 'Seed 8 vs Bye', 'Seed 6 vs Bye', 'Seed 4 vs Bye',],
    "... and bottom is the same as right"
  );


}


{
  my $t = _get_seeder(
    draw     => 32,
    seeded   => 32,
    unseeded => 0,
  );

  test_matches(
    $t, 'left',
    [
      'Seed 1 vs Seed 32',
      'Seed 15 vs Seed 18',
      'Seed 7 vs Seed 26',
      'Seed 9 vs Seed 24',
      'Seed 11 vs Seed 22',
      'Seed 5 vs Seed 28',
      'Seed 13 vs Seed 20',
      'Seed 3 vs Seed 30'
    ],
    "Weakest opponent in seeding"
  );

  test_matches(
    $t, 'right',
    [
      'Seed 2 vs Seed 31',
      'Seed 16 vs Seed 17',
      'Seed 8 vs Seed 25',
      'Seed 10 vs Seed 23',
      'Seed 12 vs Seed 21',
      'Seed 6 vs Seed 27',
      'Seed 14 vs Seed 19',
      'Seed 4 vs Seed 29'
    ],
    "... and also on the right",
  );

}

{
  my $t = _get_seeder(
    draw           => 32,
    seeded         => 32,
    unseeded       => 0,
    'weakest-seed' => 0,
  );

  test_matches(
    $t, 'left',
    [
      'Seed 1 vs Seed 17',
      'Seed 15 vs Seed 31',
      'Seed 7 vs Seed 23',
      'Seed 9 vs Seed 25',
      'Seed 11 vs Seed 27',
      'Seed 5 vs Seed 21',
      'Seed 13 vs Seed 29',
      'Seed 3 vs Seed 19'
    ],
    "Not the weakest initial seed",
  );

  test_matches(
    $t, 'right',
    [

      'Seed 2 vs Seed 18',
      'Seed 16 vs Seed 32',
      'Seed 8 vs Seed 24',
      'Seed 10 vs Seed 26',
      'Seed 12 vs Seed 28',
      'Seed 6 vs Seed 22',
      'Seed 14 vs Seed 30',
      'Seed 4 vs Seed 20'
    ],
    "... also on the right side"
  );

}
{
  my $t = _get_seeder(
    seeded     => 8,
    unseeded   => 4,
    qualifiers => 2,
    wildcards  => 2,
  );

  test_matches(
    $t,
    [
      'Seed 1 vs Unseeded 1',
      'Seed 2 vs Unseeded 2',
      'Seed 7 vs Wild card 1',
      'Seed 8 vs Wild card 2',
      'Seed 5 vs Qualifier 1',
      'Seed 6 vs Qualifier 2',
      'Seed 3 vs Unseeded 3',
      'Seed 4 vs Unseeded 4'
    ],
    "16/8 seed/4 unseeded/2 quali/2 wildcards",
  );
}

{
  my $t = _get_seeder(
    seeded     => 8,
    unseeded   => 4,
    qualifiers => 2,
    wildcards  => 2,
    order      => [qw(wildcard qualifier unseeded)]
  );

  test_matches(
    $t,
    [
      'Seed 1 vs Wild card 1',
      'Seed 2 vs Wild card 2',
      'Seed 7 vs Unseeded 3',
      'Seed 8 vs Unseeded 4',
      'Seed 5 vs Unseeded 1',
      'Seed 6 vs Unseeded 2',
      'Seed 3 vs Qualifier 1',
      'Seed 4 vs Qualifier 2'
    ],
    "... and change of order"
  );
}

{

  my $t = _get_seeder(
    draw       => 64,
    seeded     => 16,
    wildcards  => 4,
    qualifiers => 4,
  );

  test_matches(
    $t, 'left',
    [
      'Seed 1 vs Unseeded 1',
      'Unseeded 16 vs Unseeded 32',
      'Seed 15 vs Unseeded 15',
      'Unseeded 20 vs Unseeded 36',
      'Seed 7 vs Unseeded 7',
      'Unseeded 24 vs Unseeded 40',
      'Seed 9 vs Unseeded 9',
      'Unseeded 28 vs Qualifier 4',
      'Unseeded 30 vs Wild card 2',
      'Seed 11 vs Unseeded 11',
      'Unseeded 26 vs Qualifier 2',
      'Seed 5 vs Unseeded 5',
      'Unseeded 22 vs Unseeded 38',
      'Seed 13 vs Unseeded 13',
      'Unseeded 18 vs Unseeded 34',
      'Seed 3 vs Unseeded 3'
    ],
    "64 draw/16 seed/4 wc/4 quali left side"
  );

  test_matches(
    $t, 'right',
    [
      'Seed 2 vs Unseeded 2',
      'Unseeded 17 vs Unseeded 33',
      'Seed 16 vs Unseeded 16',
      'Unseeded 21 vs Unseeded 37',
      'Seed 8 vs Unseeded 8',
      'Unseeded 25 vs Qualifier 1',
      'Seed 10 vs Unseeded 10',
      'Unseeded 29 vs Wild card 1',
      'Unseeded 31 vs Wild card 3',
      'Seed 12 vs Unseeded 12',
      'Unseeded 27 vs Qualifier 3',
      'Seed 6 vs Unseeded 6',
      'Unseeded 23 vs Unseeded 39',
      'Seed 14 vs Unseeded 14',
      'Unseeded 19 vs Unseeded 35',
      'Seed 4 vs Unseeded 4'
    ],
    "... and right side too"
  );
}

{

  my $t = _get_seeder(
    draw       => 128,
    seeded     => 15,
    wildcards  => 4,
    qualifiers => 4,
  );

  test_matches(
    $t, 'left',
    [
      'Seed 1 vs Unseeded 1',
      'Unseeded 51 vs Unseeded 100',
      'Unseeded 27 vs Unseeded 76',
      'Unseeded 55 vs Unseeded 104',
      'Unseeded 15 vs Unseeded 64',
      'Unseeded 59 vs Qualifier 3',
      'Unseeded 31 vs Unseeded 80',
      'Unseeded 63 vs Wild card 3',
      'Seed 13 vs Unseeded 13',
      'Unseeded 35 vs Unseeded 84',
      'Unseeded 19 vs Unseeded 68',
      'Unseeded 39 vs Unseeded 88',
      'Seed 5 vs Unseeded 5',
      'Unseeded 43 vs Unseeded 92',
      'Unseeded 23 vs Unseeded 72',
      'Unseeded 47 vs Unseeded 96',
      'Seed 7 vs Unseeded 7',
      'Unseeded 49 vs Unseeded 98',
      'Unseeded 25 vs Unseeded 74',
      'Unseeded 45 vs Unseeded 94',
      'Seed 9 vs Unseeded 9',
      'Unseeded 41 vs Unseeded 90',
      'Unseeded 21 vs Unseeded 70',
      'Unseeded 37 vs Unseeded 86',
      'Seed 11 vs Unseeded 11',
      'Unseeded 33 vs Unseeded 82',
      'Unseeded 61 vs Wild card 1',
      'Unseeded 17 vs Unseeded 66',
      'Unseeded 57 vs Qualifier 1',
      'Unseeded 29 vs Unseeded 78',
      'Unseeded 53 vs Unseeded 102',
      'Seed 3 vs Unseeded 3'
    ],
    "128 draw/15 seeded/4wc/4quali/rest seeded left side"
  );

  test_matches(
    $t, 'right',
    [
      'Seed 2 vs Unseeded 2',
      'Unseeded 52 vs Unseeded 101',
      'Unseeded 28 vs Unseeded 77',
      'Unseeded 56 vs Unseeded 105',
      'Unseeded 16 vs Unseeded 65',
      'Unseeded 60 vs Qualifier 4',
      'Unseeded 32 vs Unseeded 81',
      'Seed 15 vs Unseeded 15',
      'Unseeded 36 vs Unseeded 85',
      'Unseeded 20 vs Unseeded 69',
      'Unseeded 40 vs Unseeded 89',
      'Seed 8 vs Unseeded 8',
      'Unseeded 44 vs Unseeded 93',
      'Unseeded 24 vs Unseeded 73',
      'Unseeded 48 vs Unseeded 97',
      'Seed 10 vs Unseeded 10',
      'Seed 12 vs Unseeded 12',
      'Unseeded 50 vs Unseeded 99',
      'Unseeded 26 vs Unseeded 75',
      'Unseeded 46 vs Unseeded 95',
      'Seed 6 vs Unseeded 6',
      'Unseeded 42 vs Unseeded 91',
      'Unseeded 22 vs Unseeded 71',
      'Unseeded 38 vs Unseeded 87',
      'Seed 14 vs Unseeded 14',
      'Unseeded 34 vs Unseeded 83',
      'Unseeded 62 vs Wild card 2',
      'Unseeded 18 vs Unseeded 67',
      'Unseeded 58 vs Qualifier 2',
      'Unseeded 30 vs Unseeded 79',
      'Unseeded 54 vs Unseeded 103',
      'Seed 4 vs Unseeded 4'
    ],
    "... and the right side"
  );
}

{

  my $t = _get_seeder(seeded => 1);
  test_matches(
    $t, 'left',
    [
      'Seed 1 vs Unseeded 1',
      'Unseeded 6 vs Unseeded 13',
      'Unseeded 4 vs Unseeded 11',
      'Unseeded 2 vs Unseeded 9'
    ],
    "1 seeded player left side",
  );
  test_matches(
    $t, 'right',
    [
      'Unseeded 3 vs Unseeded 10',
      'Unseeded 5 vs Unseeded 12',
      'Unseeded 7 vs Unseeded 14',
      'Unseeded 1 vs Unseeded 8'
    ],
    "... and right side"
  );
}

{
  my $t = _get_seeder(
    draw     => 8,
    seeded   => 6,
    unseeded => 1,
  );

  test_matches(
    $t,
    [
      'Seed 1 vs Bye',
      'Seed 2 vs Unseeded 1',
      'Seed 3 vs Seed 6',
      'Seed 4 vs Seed 5'
    ],
    "... and bye and unseeded"
  );

}


{
  my $t = _get_seeder(draw => 8, unseeded => 7, seeded => 0);
  test_matches(
    $t,
    [
      'Unseeded 1 vs Bye',
      'Unseeded 4 vs Unseeded 7',
      'Unseeded 3 vs Unseeded 6',
      'Unseeded 2 vs Unseeded 5'
    ],
    "No seeded players w/ bye",
  );
}

{
  my $t = _get_seeder(draw => 16, seeded => 4, unseeded => 5);
  test_matches(
    $t,
    [
      'Seed 1 vs Bye',
      'Seed 2 vs Bye',
      'Unseeded 4 vs Unseeded 5',
      'Unseeded 3 vs Bye',
      'Unseeded 1 vs Bye',
      'Unseeded 2 vs Bye',
      'Seed 3 vs Bye',
      'Seed 4 vs Bye'
    ],
    "Unseeded players w/ bye"
  );
}

{
  my $t = _get_seeder(
    seeded   => 6,
    unseeded => 0,
  );

  test_matches(
    $t,
    [
      'Seed 1 vs Bye',
      'Seed 2 vs Bye',
      'Bye vs Bye',
      'Bye vs Bye',
      'Seed 5 vs Bye',
      'Seed 6 vs Bye',
      'Seed 3 vs Bye',
      'Seed 4 vs Bye',
    ],
    "8 draw, 6 players, 2 bye vs bye"
  );
}

{
  my $t = _get_seeder(
    seeded   => 7,
    unseeded => 0,
  );

  test_matches(
    $t, 'left',
    ['Seed 1 vs Bye', 'Bye vs Bye', 'Seed 5 vs Bye', 'Seed 3 vs Bye',],
    "8 draw, 7 players, 1 bye vs bye"
  );
}

{
  my $t = _get_seeder(
    draw     => 32,
    seeded   => 4,
    unseeded => 6,
  );

  test_matches(
    $t, 'left',
    [
      'Seed 1 vs Bye',
      'Bye vs Bye',
      'Unseeded 5 vs Bye',
      'Bye vs Bye',
      'Unseeded 1 vs Bye',
      'Unseeded 3 vs Bye',
      'Bye vs Bye',
      'Seed 3 vs Bye',
    ],
    "BTA womens ITF madness",
  );
  test_matches(
    $t, 'right',
    [
      'Seed 2 vs Bye',
      'Bye vs Bye',
      'Unseeded 6 vs Bye',
      'Bye vs Bye',
      'Unseeded 2 vs Bye',
      'Unseeded 4 vs Bye',
      'Bye vs Bye',
      'Seed 4 vs Bye'
    ],
    "... and the right side is good too",
  );

}

done_testing;
