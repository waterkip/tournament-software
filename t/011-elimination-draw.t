use strict;
use warnings;
use Test::More 0.96;
use Test::Exception;
use Test::Deep;

use Games::Tournament::Format::Elimination::Single;


my %tests = (
  8   => { rounds => [qw(QF SF F)] },
  16  => { rounds => [qw(R16 QF SF F)] },
  32  => { rounds => [qw(R32 R16 QF SF F)] },
  64  => { rounds => [qw(R64 R32 R16 QF SF F)] },
  128 => { rounds => [qw(R128 R64 R32 R16 QF SF F)] },
  256 => { rounds => [qw(R256 R128 R64 R32 R16 QF SF F)] },
);

foreach my $ds (sort { $b <=> $a } keys %tests) {
  subtest "Draw size of $ds has correct rounds" => sub {

    my @seeds = (1 .. $ds);
    my $t     = Games::Tournament::Format::Elimination::Single->new(
      draw   => $ds,
      seeded => \@seeds,
    );

    my $rounds = $tests{$ds}{rounds};
    my $r      = @$rounds;

    is($t->rounds, $r, "Got $r rounds");
    foreach (1..$r) {
      is($t->round($_), $rounds->[$_-1], "Got $rounds->[$_-1]");
    }

    foreach (@$rounds) {
      is($t->round($_), $_, "Got the round by name: $_");
    }
  };
}

done_testing;
