use strict;
use warnings;
use Test::More 0.96;
use Test::Exception;
use Test::Deep;

use Games::Tournament::Format::Elimination::Single;

my @seeded = (1 .. 8);

my $draw = Games::Tournament::Format::Elimination::Single->new(
  draw   => 8,
  seeded => \@seeded
);
isa_ok($draw, "Games::Tournament::Format::Elimination::Single");


is($draw->number_of_rounds, 3, "Number of rounds is three");
my @rounds = $draw->rounds();
is(@rounds, 3, ".. which is three rounds");
cmp_deeply(\@rounds, [qw(QF SF F)], "All the rounds are there");

is($draw->player_count, 8, "We have (seeded) 8 players");

# QF (4), SF (2), F(1)
my @games = $draw->games();
is(@games, 7, "This draw has seven games");

{
  my $draw = Games::Tournament::Format::Elimination::Single->new(
    draw   => 8,
    seeded => [(1 .. 5)]
  );
  isa_ok($draw, "Games::Tournament::Format::Elimination::Single");

  is($draw->player_count, 5, "We have 5 players");
  is($draw->byes,         3, "We have three byes");
}

{
  my $draw = Games::Tournament::Format::Elimination::Single->new(
    draw     => 8,
    seeded   => [],
    unseeded => [(1 .. 4)]
  );

  is($draw->player_count, 4, "We have four players");
  is($draw->byes,         4, "We have four byes");

}

{
  my $draw = Games::Tournament::Format::Elimination::Single->new(
    draw   => 16,
    seeded => [(1 .. 16)]
  );
  isa_ok($draw, "Games::Tournament::Format::Elimination::Single");

  is($draw->games, 15, "Fifteen games");
}

{
  throws_ok(
    sub {
      Games::Tournament::Format::Elimination::Single->new(draw => 8, seeded => []);
    },
    qr/Cannot have a tourney without participants/,
    "Unable to start seeding without players"
  );
  throws_ok(
    sub {
      Games::Tournament::Format::Elimination::Single->new(draw => 8, unseeded => []);
    },
    qr/Cannot have a tourney without participants/,
    "Unable to start seeding without players"
  );
  throws_ok(
    sub {
      Games::Tournament::Format::Elimination::Single->new(draw => 8);
    },
    qr/No seeded or unseeded participants/,
    "Unable to start seeding without players"
  );
  throws_ok(
    sub {
      Games::Tournament::Format::Elimination::Single->new(draw => 5);
    },
    qr/Unable to set draw of 5, not a power of 2/,
    "Unable to start seeding with incorrect draw size"
  );

}




done_testing;
